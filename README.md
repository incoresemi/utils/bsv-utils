
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/utils/bsv-utils.git). It will soon be archived and eventually deleted.**

# bsv-utils
Contains utilities and docs for BSV development

# bsv2rst

## Install Tcldot package

```shell
sudo aptitude install graphviz
```

## Build the rst from bsv

```shell
python -m bsv2rst.main -p stage0 -i stage0 -d ../src/ -b ../build/hw/intermediate/ -l https://gitlab.com/incoresemi/core-generators/chromite/-/blob/master/src/stage0.bsv -o ./docs/source/stage0.rst --verbose=debug
```
