import logging
import os
import sys
import shutil
import warnings
import bsv2rst.bsv2rst as bsv2rst
import bsv2rst.utils as utils
import re
from bsv2rst import __version__ as version

logger = logging.getLogger(__name__)

def main():

    parser = utils.bsv2rst_cmdline_args()
    args = parser.parse_args()
    
    if args.version :
        print('BSV2RST: BSV documentation in RST format')
        print('Version: ' + version)
        return 0

    utils.setup_logging(args.verbose)
    logger = logging.getLogger()
    logger.handlers = []
    ch = logging.StreamHandler()
    ch.setFormatter(utils.ColoredFormatter())
    logger.addHandler(ch)
    fh = logging.FileHandler('run.log', 'w')
    logger.addHandler(fh)

    bsv_fl=f"{args.package if(args.package.strip().endswith('.bsv')) else args.package+'.bsv'}"
    bsvfile = open(args.dir+'/'+bsv_fl, 'r')
    bsv = bsvfile.read()

    global srclink
    if args.link == None:
        raise NameError(f"Provide hyperlink to remote '{bsv_fl}'. \nNOTE: use '-l skip' if not yet hosted in remote or WIP")
    else:
        if args.link == 'skip':
            srclink = args.dir+'/'+bsv_fl+'#'
            warnings.warn(f"It is mandatory to provide the Hyperlink to the remote hosted '{bsv_fl}'")
        else:
            srclink = args.link+'#'

    rst = open(args.outfile,'w')
    modul = f"{args.instance if(args.instance.strip().startswith('mk')) else 'mk'+args.instance}"
    instance = modul[2:]

    schedule = open(args.build+'/'+modul+'.sched' , 'r')
    sched = schedule.read()
    
    gen_rst_file(bsv, rst, instance, sched, srclink, args.config)
    
    bsvfile.close()
    rst.close()
    schedule.close()


def gen_rst_file(bsv, rst, instance, sched, srclink, config):
    rst.write('###################\nModule: '+instance+'\n###################\n\n')
    logger.debug('Extracting Overview')
    bsv2rst.extract_overview(bsv,rst)
    logger.debug('Extracting Compile Macros')
    bsv2rst.extract_compile_macros(bsv, rst)
    logger.debug('Extracting Imports')
    bsv2rst.extract_imports(bsv, rst)
    if 'In' in config:
        logger.debug('Extracting Interfaces')
        bsv2rst.extract_ifc(bsv, rst, instance, srclink)

    if 'Re' in config:
        logger.debug('Extracting Registers')
        bsv2rst.extract_regs(bsv, rst, srclink)
    if 'Fi' in config:
        logger.debug('Extracting FIFOs')
        bsv2rst.extract_fifos(bsv, rst, srclink)
    if 'Fu' in config:
        logger.debug('Extracting Functions')
        bsv2rst.extract_funcs(bsv, rst, srclink)
    if 'Wi' in config:
        logger.debug('Extracting Wires')
        bsv2rst.extract_wires(bsv, rst, srclink)
    if 'Ru' in config:
        logger.debug('Extracting Rules')
        bsv2rst.extract_rules(bsv, sched, rst, srclink)
    if 'No' in config:
        logger.debug('Extracting Notes')
        bsv2rst.extract_notes(bsv, rst, srclink)
    logger.debug('Generating interface image')
    bsv2rst.gen_dot_image(bsv, rst, instance)

if __name__ == "__main__":
    exit(main())
