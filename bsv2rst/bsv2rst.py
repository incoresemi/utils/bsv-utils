import os
import sys
import re
import string as string
import subprocess

def reindent(s, numSpaces):
    s = s.split('\n')
    s = [re.sub('^', ' '*numSpaces, line, re.M|re.S) for line in s]
    s = "\n".join(s)
    return s

pkg = 'icache'
instance = 'mkicache'
boolmacro_list = []
valuemacro_list = []
defaultmacros = ['ifdef', 'logLevel(', 'endif','else', 'include']

prelude = [
    'Connectable',
    'FIFOF',
    'BUtils',
    'GetPut',
    'FIFO',
    'SpecialFIFOs',
    'Clocks',
    'DReg',
    'FIFOLevel',
    'Vector',
    'Counter',
    'ConfigReg',
    'Assert',
    'BRAMCore',
    'BRAM',
    'RegFile',
    'UniqueWrappers',
    'LFSR',
    'Randomizable',
    'StmtFSM',
    'List',
    'ClientServer',
    'Probe',
    'OVLAssertions',
    'CBus',
    'ModuleCollect',
    'RevertReg',
    'MIMO',
    'List',
    'OInt',
    'Memory',
    'Cntrs',
    'GrayCounter',
    'CompletionBuffer',
    'DefaultValue',
    'TieOff',
    'ZBus',
    'CRC',
    'Real',
    'Gearbox',
    'AlignedFIFOs'
]

moduletemplate = '''
Description
{0}
'''

ruletemplate = '''
* `{0} <{4}>`_

   - **Description**:{1}
   - **Blocking Rules/Methods**: {3}
   - **Predicate**:

     .. code-block:: 

     {2}

'''

regtemplate = '''
* `{0} <{4}>`_

   - **Data Type**: ``{1}``
   - **Reset Value**: ``{2}``
   - **Description**: {3}
'''

wiretemplate = '''
* `{0} <{4}>`_

   - **Data Type**: ``{1}``
   - **Default Value**: ``{2}``
   - **Description**: {3}
'''

fifotemplate = '''
* `{0} <{5}>`_

   - **Data Type** : ``{1}``
   - **Module Type** : ``{2}``
   - **Depth of FIFO** : {3}
   - **Description**: {4}

'''

functemplate = '''
* `{0} <{4}>`_
    
   - **Description**: {3}
   - **Return type**: ``{1}``
   - **Arguments**: ``{2}``

'''

subifctemplate = '''
* `{0} <{3}>`_
    
   - **Data Type**: ``{1}``
   - **Description**:{2}

'''

methodtemplate = '''
* `{0} <{5}>`_
    
   - **Method Type**: ``{1}``
   - **Module Arguments**: ``{2}``
   - **Method Return Type**: ``{3}``
   - **Description**: {4}

'''

def clean_up_text(text):
    text = text.replace("\\", "")
    text = text.replace("\t", " ")
    text = text.replace("  ", " ")
    return text


def extract_rules(bsv, sched, rst, srclink):
    '''
    Extracts the following for each Rule:
        1. Rule Name
        2. Description of each rule
        3. Predicates for each rule
    '''
    ruleextract =\
    re.findall(r'\/\*\s*doc\s*:\s*rule\s*:[\n]*(.*?)\*\/\n\s*rule\s(.*?)\s*[\(;]', bsv, re.M|re.S)
#    re.findall(r'^\s*\(\*\s*doc\s*=\s*\"\s*rule\s*:\n(.*?)\"\s*\*\)\n(?=\s*rule\s*(.*?)\s*[\(;])', bsv, re.M|re.S)
    if len(ruleextract) != 0 :
        rst.write('\nRule Instances'+'\n--------------------')
    for r in ruleextract:
        pred = re.findall(r'^\s*Rule:\s*{0}\s*Predicate:\s*(.*?)Blocking rules: (.*?)\s[Rule|Logical]'.format(r[1]) , sched, re.M|re.S)[0]
        lineno = get_lineno(bsv, 'rule '+r[1], srclink)
        rst.write(ruletemplate.format(r[1], r[0].replace('\n',''),
            reindent(pred[0],6), pred[1], lineno))

def get_reg_resetval(reg):
    ''' 
    Extract the reset value of the register
    '''
    if 'mkRegU' in reg:
        return 'undefined'
    if 'mkReg' in reg:
        return re.findall(r'mkReg\((.+?)\)$',reg, re.M|re.S)[0]
    if 'mkCReg' in reg:
        return re.findall(r'mkCReg\(\s*\d\s*,\s*(.+?)\)$', reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_regs(bsv, rst, srclink):
    '''
    Extract the following for each Reg:
        1. Register Name
        2. Register Data Type
        3. Reset value if any
    '''
    x =\
    re.findall(r'\/\*\s*doc\s*:\s*reg\s*:[\n]*(.*?)\s*\*\/\n\s*Reg#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        rst.write('\nRegister Instances'+'\n--------------------')
    count = 0;
    for r in x:
        count = count + 1
        desc, datatype, name, resetval = r
        resetvalue = get_reg_resetval(resetval)
        lineno = get_lineno(bsv, name, srclink)
        rst.write(regtemplate.format(re.sub('\[[^\]]+\]','',name), datatype, resetvalue, 
                desc.replace('\n',''), lineno, count))

def get_wire_defaultval(reg):
    ''' 
    Extract the default value of the Wires (if any)
    '''
    if 'mkDWire' in reg:
        return re.findall(r'mkDWire\((.+?)\)$',reg, re.M|re.S)[0]
    else:
        return 'none'

def extract_wires(bsv, rst, srclink):
    '''
    Extract the following for each Wire:
        1. Wire Name
        2. Wire Data Type
        3. Default value if any
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*wire\s*:[\n]*(.*?)\s*\*\/\n\s*Wire#\(\s*(.*?)\)\s+(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        rst.write('\nWire Instances'+'\n--------------------')
    count = 0
    for r in x:
        count = count + 1
        desc, datatype, name, resetval = r
        resetvalue = get_wire_defaultval(resetval)
        lineno = get_lineno(bsv, name, srclink)
        rst.write(wiretemplate.format(re.sub('\[[^\]]+\]','',name), datatype,
            resetvalue, desc.replace('\n',''), lineno, str(count)))

def extract_fifodepth(fifo):
    '''
    Extract the fifo module type and the depth of the fifo
    '''
    name, size = re.findall(r'(mk.*FIFO.*?)\s*\((.*?)\)', fifo, re.M|re.S)[0]
    if size is '':
        size = 1
    return name, size

def extract_fifos(bsv, rst, srclink):
    '''
    Extract the following for each FIFO
    1. FIFO instance name
    2. FIFO Data type
    3. FIFO Module 
    4. FIFO Size
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*fifo\s*:[\n]*(.*?)\s*\*\/\n\s*FIFO[F]#\(\s*(.*?)\)+\s*(.*?)\s*<-\s*(.*?);', bsv, re.M|re.S)
    if len(x) != 0:
        rst.write('\nFIFO Instances'+'\n--------------------')
    count = 0
    for r in x:
        count = count + 1
        desc, datatype, name, fifoinfo = r
        fifotype, size = extract_fifodepth(fifoinfo)
        lineno = get_lineno(bsv, name, srclink)
        rst.write(fifotemplate.format(re.sub('\[[^\]]+\]','',name), datatype,
            fifotype,  size, desc.replace('\r',''), lineno, count))

def extract_funcs(bsv, rst, srclink):
    '''
    Extract the following for each function
    1. function name
    2. function Return Type
    3. function Args (if any)

    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*func\s*:[\n]*(.*?)\s*\*\/\n\s*function\s*(.*?)\s*(fn_.*?)\((.*?)\)\s*[=;]', bsv, re.M|re.S)
    if len(x) != 0:
        rst.write('\nLocal functions'+'\n-------------------')
    for r in x:
        lineno = get_lineno(bsv, r[2], srclink)
        rst.write(functemplate.format(r[2], r[1], re.sub('\s+',' ',r[3]).rstrip(), reindent(r[0], 6), lineno))

def extract_ifc(bsv, rst, instance, srclink):
    ''' 
    Extract the following for each subinterface used:
    1. Interface Name
    2. Interface Data type
    3. Description
    '''

    subifc = re.findall(r'\/\*\s*doc\s*:\s*subifc\s*:[\n]*(.*?)\s*\*\/\n\s*interface\s*(.*?)\s+(.*?)[=;]', bsv, re.M|re.S)
    method = re.findall(r'\/\*\s*doc\s*:\s*method\s*:[\n]*(.*?)\s*\*\/\n\s*method\s*(.*?)\s+([ma mv mav].*?)\s*\(\s*(.*?)\)\s*;', bsv, re.M|re.S)
    
    if len(subifc) != 0 or len(method) != 0:
        rst.write('\nInterface'+'\n--------------------------\n')
        rst.write('''
.. image:: {0}.png
    :align: center
    :alt: testStruct
'''.format(instance))
    count = 0
    for r in subifc:
        count = count + 1
        lineno = get_lineno(bsv, r[2], srclink)
        rst.write(subifctemplate.format(r[2], r[1], r[0].replace('\n',''), lineno, count))
    for r in method:
        count = count + 1
        desc, methodtype, name, arguments = r
        returntype = 'none'
        lineno = get_lineno(bsv, name, srclink)
        if arguments is '':
            arguments = 'none'
        if 'ActionValue' in methodtype:
            returntype = re.findall(r'ActionValue#\((.*?)\)$', methodtype, re.M|re.S)[0]
            methodtype = 'ActionValue'
        rst.write(methodtemplate.format(name, methodtype, arguments, returntype, 
            desc.replace('\n',''), lineno, count))    

def extract_imports(bsv, rst):
    '''
    Extract the list of library and project imports
    '''
    x = re.findall(r'^\s*import\s*(.*?)\s*::\s*\*\s*;', bsv, re.M|re.S)
    rst.write('\n\nLibrary Imports\n----------------\n\n')
    rst.write('\n - **Prelude Library Imports**:\n\n')
    count = 1
    for r in x:
        if r in prelude:
            rst.write('   * '+r+'\n')
            count = count+1
    rst.write('\n - **Porject Library Imports**:\n\n')
    count = 1
    for r in x:
        if r not in prelude:
            rst.write('   * '+r+'\n')
            count = count+1

def extract_notes(bsv, rst, srclink):
    '''
    Extract notes if any
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*note\s*:\s*(.*?)\s*\*\/', bsv, re.M|re.S)
    count = 1
    for r in x:
        rst.write('.. note:: '+r)

def extract_module(bsv, rst):
    ''' 
    Extract module description
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*module\s*:\s*\n(.*?)\*\/\n(?=\s*module)', bsv, re.M|re.S)
    for r in x:
        rst.write((moduletemplate.format(r)))

def extract_compile_macros(bsv, rst):
    '''
    Extract the list of the ifdef macros that can be enabled/disabled at compile
    time
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*macros\s*:\s*(.*?)\s*\*\/',bsv, re.M|re.S)
    if len(x) != 0:
        rst.write('\nCompile Macros\n------------------\n\n')
        rst.write(x[0])

def extract_overview(bsv, rst):
    ''' 
    Extract overview
    '''
    x = re.findall(r'\/\*\s*doc\s*:\s*overview\s*:\s*\n(.*?)\*\/', bsv, re.M|re.S)
    for r in x:
        rst.write(r)

def get_lineno(bsv, text, srclink):
    lineno = 1
    x = bsv.find(text)
    if x is not -1:
        for i in range(0, len(bsv)):
            if '\n' in bsv[i]:
                lineno = lineno + 1
            if i == x:
                return srclink+'L'+str(lineno)

def gen_dot_image(bsv, rst, instance):
    
    block = {
        'module_name': '',
        'input_count': 0,
        'output_count': 0,
        'inputs':[],
        'outputs':[]
        }

    temp = r'interface\s*Ifc_{0}(.*?)endinterface\s*:\s*Ifc_{0}'.format(instance)
    ifc_text = re.findall(temp, bsv, re.M|re.S)[0]
    method_action =\
            re.findall(r'\s*method\s+Action\s+(.*?)(?=[\(.*\)\s*;])',ifc_text,re.M)
    method_value = \
            re.findall(r'^\s*method\s+.*(mv.*?)(?=[\(.*\)\s*;])',ifc_text,re.M)
    method_put = \
            re.findall(r'^\s*interface\sPut#+.*(put_.*?)(?=[\(.\)\s;])',ifc_text, re.M)
    method_get = \
            re.findall(r'^\s*interface\sGet#+.*(get_.*?)(?=[\(.\)\s;])',ifc_text, re.M)

    for ma in method_action:
        block['input_count'] = block['input_count'] + 1
        block['inputs'].append(ma)
    for mv in method_value:
        block['output_count'] = block['output_count'] + 1
        block['outputs'].append(mv)
    for put in method_put:
        block['input_count'] = block['input_count'] + 1
        block['inputs'].append(put)
    for get in method_get:
        block['output_count'] = block['output_count'] + 1
        block['outputs'].append(get)
    block['module_name'] = instance

    dot_file = open('{}.dot'.format(instance), 'w')
    dot_file.write('digraph G {\n')
    dot_file.write('\trankdir=LR;\n')
    dot_file.write('\tsplines=\"line\";\n')
    #dot_file.write('\tbgcolor=\"transparent\";\n')
    color = '#DAE8FC'
    arrow_color = '#303E6B'
    port_count = max(block['input_count'], block['output_count'])
    module_name_pos = int(port_count/2) - 1 
    port_string = '';
    for i in range(port_count):
        if i == module_name_pos:
            if i == 0:
                port_string = '<port{0}> {1} '.format(i, block['module_name'])
            else:
                port_string = port_string + '| <port{0}> {1} '.format(i, block['module_name'])
        else :
            if i == 0: 
                port_string = '<port{}> '.format(i)
            else:
                port_string = port_string + '| <port{}>'.format(i)

    dot_file.write('\tmodule [label=\"{}\",\n'.format(port_string))
    dot_file.write('\t\tshape=record, style=filled, color=\"{0}\", fillcolor=\"{0}\"\n'.format(color))
    dot_file.write('\t\twidth=2, height={}];\n'.format(port_count))
    for i in range(block['input_count']):
        dot_file.write('\tinput_intf{} [style=\"invis\"];\n'.format(i))
    
    for i in range(block['output_count']):
        dot_file.write('\toutput_intf{} [style=\"invis\"];\n'.format(i))  

    for i in range(block['input_count']):
        dot_file.write('\tinput_intf{0} -> module:port{0} [label=\"{1}\", penwidth=3.0, color=\"{2}\"];\n'.format(i,block['inputs'][i], arrow_color))

    for i in range(block['output_count']):
        dot_file.write('\tmodule:port{0} -> output_intf{0} [label=\"{1}\", penwidth=3.0, color=\"{2}\"];\n'.format(i,block['outputs'][i], arrow_color))
    dot_file.write('}\n')
    dot_file.close()
    subprocess.run('dot -Tpng {0}.dot -o {0}.png'.format(instance).split(), stdout=subprocess.PIPE)
